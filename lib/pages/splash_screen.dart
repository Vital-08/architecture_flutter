import 'package:flutter/material.dart';

import 'sign_in.dart';

@immutable
class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  // ignore: library_private_types_in_public_api
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();

    Future.delayed(const Duration(seconds: 3), () {
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => const SignIn()));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        //Centre le contenu de la page sur l'écran

        child: Image.asset(
            'assets/images/logo.png'), //Affiche le logo de l'application
      ),
    );
  }
}
