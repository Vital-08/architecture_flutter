import 'package:flutter/material.dart';

// nom de la police
const String font = 'DayRoman';
const kPrimaryColor = Color.fromRGBO(64, 81, 230, 1);
const ksecondaryColor = Color.fromRGBO(255, 100, 0, 1);
const kTeXTColor = Color.fromARGB(255, 0, 0, 0);
const kBackgroundColor = Color(0xFFF9F8FD);
const kHeaderColor = Color(0xFFF5F5F5);
IconThemeData themeIcon = const IconThemeData(color: Colors.black);
const double kDefaultPadding = 20.0;
const double radiusBorder = 10.0;
